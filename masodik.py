import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from collections import OrderedDict
from tensorflow.keras.models import load_model
from scipy import spatial
from collections import Counter


def one_hot_word(w, word_map):
    ohe = []
    for _ in range(word_map[w]):
        ohe.append(0)
    ohe.append(1)
    while len(ohe) != len(word_map):
        ohe.append(0)
    return np.asarray(ohe)


stop_words = set(stopwords.words('english'))
tk = RegexpTokenizer(r'\w+')


words = dict()
appearances = dict()
df = pd.read_csv("../train/train.csv", index_col=0)

idx = -1
for index, row in df.iterrows():
    row['text'] = row['text'].lower()
    for word in tk.tokenize(row['text']):
        if (not str(word).isnumeric()) and (word not in stop_words):
            if word not in appearances:
                appearances[word] = 1
            else:
                appearances[word] += 1

appearances_sorted = OrderedDict(sorted(appearances.items(), key=lambda item: item[1], reverse=True))

for key, value in appearances_sorted.items():
    idx += 1
    if idx > 99:
        words[key] = idx - 100
    if idx >= 10099:
        break

# print(words)

pairs = []

for index, row in df.iterrows():
    sentence = tk.tokenize(row['text'])
    for i in range(len(sentence)):
        word = sentence[i]
        if word in words:
            sub_i = max(0, i - 3)
            top_i = min(len(sentence), i + 3)
            for j in range(sub_i, top_i):
                if j != i:
                    word2 = sentence[j]
                    if word2 in words:
                        pairs.append([word, word2])

# print(pairs)

model = load_model('model10')

weights = np.asarray(model.layers[1].get_weights()[0])

df2 = pd.read_csv('combined.csv')

result_list = []
mse = 0

for index, row in df2.iterrows():
    w1 = row['Word 1']
    w2 = row['Word 2']
    if (w1 in words) and (w2 in words):
        idx1 = words[w1]
        idx2 = words[w2]
        cos_sim = 1 - spatial.distance.cosine(weights[:, idx1], weights[:, idx2])
        result_list.append([w1, w2, max(0, cos_sim)])
        mse += (max(0, cos_sim) - row['Human (mean)'] / 10) ** 2

mse /= len(result_list)

print('Mean Squared Error Model: ' + str(mse))
# print(sorted(result_list, key=lambda x: x[2]))

mse = 0
ll = []
for key, value in words.items():
    word = key
    minilist = []
    for index, row in df.iterrows():
        c = Counter(row['text'])
        minilist.append(c.get(key, 0))
    ll.append(minilist)

for index, row in df2.iterrows():
    w1 = row['Word 1']
    w2 = row['Word 2']
    if (w1 in words) and (w2 in words):
        idx1 = words[w1]
        idx2 = words[w2]
        cos_sim = 1 - spatial.distance.cosine(ll[words[w1]], ll[words[w2]])
        result_list.append([w1, w2, max(0, cos_sim)])
        mse += (max(0, cos_sim) - row['Human (mean)'] / 10) ** 2


print('Mean Squared Error Bag: ' + str(mse))
