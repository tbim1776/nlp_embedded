import re
import string
import numpy as np
import pandas as pd
import tensorflow as tf
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense
from collections import OrderedDict


def one_hot_word(w, word_map):
    ohe = []
    for _ in range(word_map[w]):
        ohe.append(0)
    ohe.append(1)
    while len(ohe) != len(word_map):
        ohe.append(0)
    return np.asarray(ohe)


class MySequence(tf.keras.utils.Sequence):
    def __init__(self, X, y, batch_size, word_map):
        self.X, self.y = X, y
        self.batch_size = batch_size
        self.word_map = word_map

    def __len__(self):
        return int(np.ceil(len(self.X) / float(self.batch_size)))

    def __getitem__(self, batch_id):
        batch_X = self.X[batch_id * self.batch_size:(batch_id + 1) * self.batch_size]
        batch_y = self.y[batch_id * self.batch_size:(batch_id + 1) * self.batch_size]
        return (np.array([one_hot_word(w, self.word_map) for w in batch_X]),
                np.array([one_hot_word(w, self.word_map) for w in batch_y]))


stop_words = set(stopwords.words('english'))
tk = RegexpTokenizer(r'\w+')

words = dict()
appearances = dict()
df = pd.read_csv("../train/train.csv", index_col=0)

for index, row in df.iterrows():
    row['text'] = row['text'].lower()
    for word in tk.tokenize(row['text']):
        if (not str(word).isnumeric()) and (word not in stop_words):
            if word not in appearances:
                appearances[word] = 1
            else:
                appearances[word] += 1

appearances_sorted = OrderedDict(sorted(appearances.items(), key=lambda item: item[1], reverse=True))

idx = -1

for key, value in appearances_sorted.items():
    idx += 1
    if idx > 99:
        words[key] = idx - 100
    if idx >= 10099:
        break

# print(words)

pairs = []

for index, row in df.iterrows():
    sentence = tk.tokenize(row['text'])
    for i in range(len(sentence)):
        word = sentence[i]
        if word in words:
            sub_i = max(0, i - 3)
            top_i = min(len(sentence), i + 3)
            for j in range(sub_i, top_i):
                if j != i:
                    word2 = sentence[j]
                    if word2 in words:
                        pairs.append([word, word2])

# print(pairs)

# Vocabulary size and number of words in a sequence.
vocab_size = 10000
sequence_length = 100
batch_size = 100
epochs = 10
embedding_dim = 50

seq = MySequence([w[0] for w in pairs], [w[1] for w in pairs], batch_size, words)

model = Sequential([
    # 10k adat -> 50 -> 10k
    Dense(embedding_dim),
    # 10 000 * 50 matrix = suly_matrix
    Dense(vocab_size, activation="Softmax")
])


model.compile(optimizer=tf.keras.optimizers.Adam(),
              loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])


cp_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath='/home/trinfa/Desktop/Egyetem/NLP/embedded/model/checkpoints/model_weights',
    verbose=1,
    save_weights_only=True)

model.fit_generator(generator=seq, epochs=epochs, callbacks=cp_callback)

print(model.get_weights())

model.save_weights('/home/trinfa/Desktop/Egyetem/NLP/embedded/model/')

model.save('model10')
